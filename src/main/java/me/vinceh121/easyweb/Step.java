package me.vinceh121.easyweb;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Step {
	private int image, pos;
	private char effect;

	public int getImage() {
		return this.image;
	}

	public void setImage(final int image) {
		this.image = image;
	}

	public int getPosition() {
		return this.pos;
	}

	public void setPosition(final int pos) {
		this.pos = pos;
	}

	@JsonProperty("effet")
	public char getEffect() {
		return this.effect;
	}

	@JsonProperty("effet")
	public void setEffect(final char effect) {
		this.effect = effect;
	}
}
