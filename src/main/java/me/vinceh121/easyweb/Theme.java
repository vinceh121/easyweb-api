package me.vinceh121.easyweb;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Theme {
	private int score, maxScore;
	private String cdth;

	public int getScore() {
		return this.score;
	}

	public void setScore(final int score) {
		this.score = score;
	}

	@JsonProperty("max")
	public int getMaxScore() {
		return this.maxScore;
	}

	@JsonProperty("max")
	public void setMaxScore(final int maxScore) {
		this.maxScore = maxScore;
	}

	public String getCdth() {
		return this.cdth;
	}

	public void setCdth(final String cfth) {
		this.cdth = cfth;
	}
}
