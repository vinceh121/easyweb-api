package me.vinceh121.easyweb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EasyWeb {
	private final ObjectMapper mapper;
	private final CloseableHttpClient client;
	private final BasicCookieStore cookieJar;

	public EasyWeb() {
		this("vinceh121's EasyWeb API");
	}

	/**
	 *
	 * @param ua The User-Agent to use
	 */
	public EasyWeb(final String ua) {
		this(ua, new ObjectMapper());
	}

	public EasyWeb(final String ua, final ObjectMapper mapper) {
		this.mapper = mapper;
		this.cookieJar = new BasicCookieStore();
		this.client = HttpClients.custom().setUserAgent(ua).setDefaultCookieStore(this.cookieJar).build();
	}

	public void ping() throws IOException {
		final HttpGet get = new HttpGet("https://public.codesrousseau.fr");
		this.client.execute(get);
	}

	/**
	 *
	 * @param username
	 * @param password
	 * @param remember
	 * @return true if login succeeded, false otherwise
	 * @throws IOException
	 */
	public boolean login(final String username, final String password, final boolean remember) throws IOException {
		final HttpPost post = new HttpPost("https://eleve.codesrousseau.fr/vues/acces_eleve/connexion.php");
		final ArrayList<BasicNameValuePair> args = new ArrayList<BasicNameValuePair>();
		args.add(new BasicNameValuePair("login", username));
		args.add(new BasicNameValuePair("mdp", "")); // XXX: Always empty?
		args.add(new BasicNameValuePair("passpermis", "log")); // TODO: Figure out what this is
		args.add(new BasicNameValuePair("methode", "identifierEleve"));
		args.add(new BasicNameValuePair("type", "html"));
		args.add(new BasicNameValuePair("parametres",
				this.mapper.createObjectNode()
						.put("remember", remember)
						.put("identifiant", username)
						.put("password", DigestUtils.md5Hex(password))
						.toString()));

		post.setEntity(new UrlEncodedFormEntity(args));
		post.setHeader("Referer", "https://public.codesrousseau.fr/");
		final CloseableHttpResponse res = this.client.execute(post);
		res.close();
		return "/vues/acces_eleve/".equals(res.getFirstHeader("Location").getValue());
	}

	public int getNumberOfMails() throws IOException {
		final JsonNode res = this.executeJson("getApplicationUpdate_eleve",
				this.mapper.createObjectNode().put("messages", true).put("majApp", true).toString());
		if (!res.get("ok").asBoolean()) {
			throw new IOException("ok = false");
		}
		return res.get("retour").get("majMessagerie").get("messages").asInt();
	}

	public void close() throws IOException {
		this.client.close();
	}

	public Series getSeries(final int number, final int question, final int type, final int theme,
			final boolean buttonMail, final int mode) throws ClientProtocolException, IOException {
		// {"numero":3,"question":1,"type":1,"code_theme":0,"btn_mail":true,"mode":0}
		final JsonNode res = this.executeJson("ew_donnerInfosSerie",
				this.mapper.createObjectNode()
						.put("numero", number)
						.put("question", question)
						.put("type", type)
						.put("code_theme", 0)
						.put("btn_mail", buttonMail)
						.put("mode", mode)
						.toString());
		if (!res.get("ok").asBoolean()) {
			return null;
		}
		return this.mapper.treeToValue(res.get("retour").get("serie"), Series.class);
	}

	public Question getQuestion(final String id) throws ParseException, ClientProtocolException, IOException {
		final HttpGet get = new HttpGet(
				"https://eleve.codesrousseau.fr/medias/easyweb/voiture/" + EasyWeb.hashPath(id) + id + ".json");
		return this.mapper.readValue(EntityUtils.toString(this.client.execute(get).getEntity()), Question.class);
	}

	/**
	 *
	 * @return Returned URL upon logout
	 * @throws IOException
	 */
	public String logout() throws IOException {
		final JsonNode res = this.executeJson("deconnecter", "");
		if (res.get("ok").asBoolean()) {
			return res.get("retour").get("url").asText();
		} else {
			throw new IOException("Returned JSON: ok == false");
		}
	}

	public String getProgressHTML() throws IOException {
		final HttpResponse res = this.execute("ew_getScoresProgressionTheoriqueEleve",
				this.mapper.createObjectNode()
						.put("step_init", 0)
						.put("step_more", 3)
						.put("scrollT", 0)
						.put("progression", "test")
						.toString(),
				"html");
		return EntityUtils.toString(res.getEntity());
	}

	public JsonNode executeJson(final String method, final String parameters) throws IOException {
		final HttpResponse res = this.execute(method, parameters, "json");
		final String str = EntityUtils.toString(res.getEntity());
		return this.mapper.readTree(str);
	}

	public HttpResponse execute(final String method, final String parameters, final String type)
			throws ClientProtocolException, IOException {
		final HttpPost post = new HttpPost("https://eleve.codesrousseau.fr/vues/acces_eleve/easyweb/traitement.php");

		final UrlEncodedFormEntity content = new UrlEncodedFormEntity(
				Arrays.asList(new BasicNameValuePair("methode", method),
						new BasicNameValuePair("parametres", parameters), new BasicNameValuePair("type", type)),
				"UTF-8");
		post.setEntity(content);
		return this.client.execute(post);
	}

	public static String hashPath(final String fileName) {
		String path = "000000" + fileName;
		path = path.substring(path.length() - 6);
		path = path.substring(0, 2) + "/" + path.substring(2, 4) + "/" + path.substring(4, 6) + "/";
		return path;
	}

	public static byte[] decodeCrf(final byte[] input) {
		final byte[] output = new byte[input.length];
		for (int i = 0; i < input.length; i++) {
			output[i] = (byte) (input[input.length - i - 1] ^ 255);
		}
		return output;
	}
}
