package me.vinceh121.easyweb;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Series {
	private String id, title;
	private int type, score, maxScore;
	// private List<Question> questions;
	private List<Theme> themes;

	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public int getType() {
		return this.type;
	}

	public void setType(final int type) {
		this.type = type;
	}

	public int getScore() {
		return this.score;
	}

	public void setScore(final int score) {
		this.score = score;
	}

	@JsonProperty("max")
	public int getMaxScore() {
		return this.maxScore;
	}

	@JsonProperty("max")
	public void setMaxScore(final int maxScore) {
		this.maxScore = maxScore;
	}

	/*
	 * public List<Question> getQuestions() {
	 * return questions;
	 * }
	 *
	 * public void setQuestions(List<Question> questions) {
	 * this.questions = questions;
	 * }
	 */

	public List<Theme> getThemes() {
		return this.themes;
	}

	public void setThemes(final List<Theme> themes) {
		this.themes = themes;
	}

	@JsonProperty("libelle")
	public String getTitle() {
		return this.title;
	}

	@JsonProperty("libelle")
	public void setTitle(final String title) {
		this.title = title;
	}
}
