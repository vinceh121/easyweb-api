package me.vinceh121.easyweb;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Question implements EasyWebEntity {
	private String id;
	private char[] answers = new char[4];

	/**
	 * idk wth this is
	 */
	private String gender;
	private List<String> question = Collections.emptyList();
	private List<String> suggestions = Collections.emptyList();
	private List<Sequence> sequences = Collections.emptyList();

	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	@JsonProperty("response")
	public char[] getAnswers() {
		return this.answers;
	}

	@JsonProperty("response")
	public void setAnswers(final char[] answers) {
		this.answers = answers;
	}

	@JsonProperty("sexe")
	public String getGender() {
		return this.gender;
	}

	@JsonProperty("sexe")
	public void setGender(final String gender) {
		this.gender = gender;
	}

	@JsonProperty("enonce")
	public List<String> getQuestion() {
		return this.question;
	}

	@JsonProperty("enonce")
	public void setQuestion(final List<String> question) {
		this.question = question;
	}

	@JsonProperty("propositon")
	public List<String> getSuggestions() {
		return this.suggestions;
	}

	@JsonProperty("propositon")
	public void setSuggestions(final List<String> suggestions) {
		this.suggestions = suggestions;
	}

	@JsonProperty("sequence")
	public List<Sequence> getSequences() {
		return this.sequences;
	}

	@JsonProperty("sequence")
	public void setSequences(final List<Sequence> sequences) {
		this.sequences = sequences;
	}
}
