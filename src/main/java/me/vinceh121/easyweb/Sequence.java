package me.vinceh121.easyweb;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Sequence {
	private String sound;
	private int time;
	private List<Image> images;
	private List<Step> steps;

	@JsonProperty("son")
	public String getSound() {
		return this.sound;
	}

	@JsonProperty("son")
	public void setSound(final String sound) {
		this.sound = sound;
	}

	@JsonProperty("temps")
	public int getTime() {
		return this.time;
	}

	@JsonProperty("temps")
	public void setTime(final int time) {
		this.time = time;
	}

	@JsonProperty("image")
	public List<Image> getImages() {
		return this.images;
	}

	@JsonProperty("image")
	public void setImages(final List<Image> images) {
		this.images = images;
	}

	@JsonProperty("etape")
	public List<Step> getSteps() {
		return this.steps;
	}

	@JsonProperty("etape")
	public void setSteps(final List<Step> steps) {
		this.steps = steps;
	}
}
