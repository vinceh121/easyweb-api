package tests;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Test;

import me.vinceh121.easyweb.EasyWeb;

public class TestDecoding {

	@Test
	public void testCrf() throws IOException {
		final FileInputStream ins = new FileInputStream("003025_BT_Q01.crf");
		final byte[] in = new byte[ins.available()];
		ins.read(in);
		ins.close();
		final byte[] out = EasyWeb.decodeCrf(in);
		final FileOutputStream outs = new FileOutputStream("testCrfout");
		outs.write(out);
		outs.close();
	}

}
