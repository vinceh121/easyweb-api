package tests;

import java.awt.HeadlessException;
import java.io.IOException;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.junit.Assert;
import org.junit.Test;

import me.vinceh121.easyweb.EasyWeb;

public class TestApi {
	private final EasyWeb web;

	public TestApi() throws HeadlessException, IOException {
		this.web = new EasyWeb("Mozilla/5.0 (X11; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0");
		this.web.ping();
		Assert.assertTrue("Login", this.web.login("", "", false)); // TODO
	}

	@Test
	public void testMails() throws IOException {
		System.out.println("Number of mails: " + this.web.getNumberOfMails());
	}

	@Test
	public void testQuestion() throws ParseException, ClientProtocolException, IOException {
		System.out.println("" + this.web.getQuestion("011424").toString());
	}

}
